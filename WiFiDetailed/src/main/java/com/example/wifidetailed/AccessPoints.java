package com.example.wifidetailed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vgajula on 6/10/13.
 */
public class AccessPoints extends Fragment  {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = "WiFiConn.WiFiDemo";
    AccessPointListViewAdapter adapter;
    boolean started = false;

    public AccessPoints() { }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        View rootView;
        rootView = inflater.inflate(R.layout.access_points, container, false);
        return rootView;
    }
}
