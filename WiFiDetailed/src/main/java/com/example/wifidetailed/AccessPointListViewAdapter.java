package com.example.wifidetailed;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by vgajula on 7/9/13.
 */
public class AccessPointListViewAdapter extends BaseAdapter {
    private final static String TAG = "WiFiConn.AccessPointListViewAdapter";
    private Activity activity;
    private List<APInfo> data;
    private static LayoutInflater inflater=null;
    private final static ArrayList<Integer> channelsFrequency =
            new ArrayList<Integer>(Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484));

    private final static int start_frequency_5ghz = 5180;
    private final static int start_channel_5ghz = 36;

    public AccessPointListViewAdapter(Activity a, List<APInfo> d) {
       // Log.d(TAG, "start AccessPointListViewAdapter");
        activity = a;
        data=d;
       // Log.d(TAG, "start AccessPointListViewAdapter 11");
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Log.d(TAG, "end AccessPointListViewAdapter");
    }

    public int getCount() {
       // Log.d(TAG, "getCount");
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
       //  Log.d(TAG, "getView");
        View vi=convertView;
        if(convertView==null)
            vi = inflater.inflate(R.layout.list_row, null);

        TextView title = (TextView)vi.findViewById(R.id.title); // title
        TextView sub_title = (TextView)vi.findViewById(R.id.sub_title); // artist name
        TextView duration = (TextView)vi.findViewById(R.id.duration); // duration

        //Log.d(TAG, "====WifiConn size:" + WifiConn.size());

        //list.add(WifiConn.get(position).ssid +  "  -  " + WifiConn.get(position).count);

        // Setting all values in listview
        title.setText(data.get(position).vendor + "\n" + data.get(position).bssid);
        //String allMacs = "AP Count: " + data.get(position).ap_count;
            /*
            String band;
            int channel;
            for(int i=0; i<data.get(position).radio.size(); i++) {
                channel = channelsFrequency.indexOf(Integer.valueOf(data.get(position).radio.get(i).frequency));
                if ( channel > 0)
                    band = "Band: 2.4 GHz Channel: " + channel;
                else {

                    channel = start_channel_5ghz + (data.get(position).radio.get(i).frequency-start_frequency_5ghz)/5;
                    band = "Band: 5 GHz Channel: " + channel;
                }
                allMacs = allMacs + "\n" + data.get(position).radio.get(i).bssid + " " + band;
            }
            */
        String extra_text = "SSID: " + data.get(position).ssid + " Frequency: "
                                + data.get(position).frequency + " Channel: "
                                + data.get(position).channel;

        sub_title.setText(extra_text);
        // artist.setText("AP Count: " + data.get(position).count +
        //    "\nMAC: " + data.get(position).radio.get(0).bssid);

        duration.setText("1:00");

        return vi;
    }
}
