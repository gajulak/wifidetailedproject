package com.example.wifidetailed;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.ScanResult;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by vgajula on 6/11/13.
 */

public class LazyAdapter extends BaseAdapter {
        private final static String TAG = "WiFiConn.LazyAdapter";
        private Activity activity;
        private List<SSIDInfo> data;
        private static LayoutInflater inflater=null;

        public LazyAdapter(Activity a, List<SSIDInfo> d) {
           // Log.d(TAG, "start LazyAdapter");
            activity = a;
            data=d;
            inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          //  Log.d(TAG, "end LazyAdapter");
        }

        public int getCount() {
           // Log.d(TAG, "getCount");
            return data.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
           // Log.d(TAG, "getView");
            View vi=convertView;
            if(convertView==null)
                vi = inflater.inflate(R.layout.list_row, null);

            TextView title = (TextView)vi.findViewById(R.id.title); // title
            TextView artist = (TextView)vi.findViewById(R.id.sub_title); // artist name
            TextView duration = (TextView)vi.findViewById(R.id.duration); // duration

            //Log.d(TAG, "====WifiConn size:" + WifiConn.size());

            //list.add(WifiConn.get(position).ssid +  "  -  " + WifiConn.get(position).count);

            // Setting all values in listview
            title.setText(data.get(position).ssid);
            String allMacs = "AP Count: " + data.get(position).ap_count;
            /*
            String band;
            int channel;
            for(int i=0; i<data.get(position).radio.size(); i++) {
                channel = channelsFrequency.indexOf(Integer.valueOf(data.get(position).radio.get(i).frequency));
                if ( channel > 0)
                    band = "Band: 2.4 GHz Channel: " + channel;
                else {

                    channel = start_channel_5ghz + (data.get(position).radio.get(i).frequency-start_frequency_5ghz)/5;
                    band = "Band: 5 GHz Channel: " + channel;
                }
                allMacs = allMacs + "\n" + data.get(position).radio.get(i).bssid + " " + band;
            }
            */
            artist.setText(allMacs);
            // artist.setText("AP Count: " + data.get(position).count +
                        //    "\nMAC: " + data.get(position).radio.get(0).bssid);

            duration.setText("1:00");

            return vi;
        }




}
