package com.example.wifidetailed;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NavUtils;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wifidetailed.WiFiCollectorService;

public class MainActivity extends FragmentActivity {
    private static final String TAG = "WiFiConn.Main";
    SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    private WiFiCollectorReceiver receiver;
    AccessPointListViewAdapter ap_adapter;
    SSIDBasedListViewAdapter ssid_adapter;
    List<SSIDInfo> SSIDInfoList = new ArrayList<SSIDInfo>();
    List<APInfo> APInfoList = new ArrayList<APInfo>();
    DataBaseHelper myDbHelper;

    private MenuItem startScan;
    private MenuItem stopScan;
    boolean start_scan_on_boot = false;
    boolean scan_running = false;

    //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(Settings.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.activity_main);

        myDbHelper = new DataBaseHelper(getApplicationContext());
        try {
            myDbHelper.createDataBase();
        } catch (IOException ioe) {
            throw new Error("Unable to create database");
        }
        myDbHelper = null;


        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        IntentFilter filter = new IntentFilter(WiFiCollectorReceiver.PROCESS_RESPONSE);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new WiFiCollectorReceiver();
        registerReceiver(receiver, filter);

        ap_adapter = new AccessPointListViewAdapter(this, APInfoList);
        ssid_adapter = new SSIDBasedListViewAdapter(this, SSIDInfoList);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        start_scan_on_boot = prefs.getBoolean("scanOnStartCheckbox", false);
        // Start WifiCollector Service
     //   stopService(new Intent(getBaseContext()), WiFiCollectorService.class);
        //GVK startService(new Intent(getBaseContext(), WiFiCollectorService.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        startScan = menu.getItem(0);
        stopScan =  menu.getItem(1);
        if (start_scan_on_boot == true) {
            startScan.setEnabled(false);
            stopScan.setEnabled(true);
        } else {
            startScan.setEnabled(true);
            stopScan.setEnabled(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.action_start_scan: {
                Intent intent = new Intent(getBaseContext(), WiFiCollectorService.class);
                intent.putExtra(WiFiCollectorService.REQUEST_STRING, "START_SCAN");
                setProgressBarIndeterminateVisibility(true);
                startScan.setEnabled(false);
                stopScan.setEnabled(true);
                scan_running = true;
                startService(intent);
                Log.d(TAG, "StartScan");
                return true;
            }
            case R.id.action_stop_scan: {
                Intent intent = new Intent(getBaseContext(), WiFiCollectorService.class);
                intent.putExtra(WiFiCollectorService.REQUEST_STRING, "STOP_SCAN");
                stopService(intent);
                setProgressBarIndeterminateVisibility(false);
                stopScan.setEnabled(false);
                startScan.setEnabled(true);
                scan_running = false;
                Log.d(TAG, "StopScan");
                return true;
            }
            case R.id.action_clear_all: {
                SSIDInfoList.clear();
                APInfoList.clear();
                ssid_adapter.notifyDataSetChanged();
                ap_adapter.notifyDataSetChanged();
                return true;
            }
            case R.id.action_settings: {
                Intent settings_intent = new Intent(MainActivity.this, Settings.class);
                MainActivity.this.startActivity(settings_intent);
            }
        }
        return false;
    }


    @Override
    public void onDestroy() {
        Log.d(TAG, "In Destroy");

       // if (isServiceRunning() == true) {
           //startScan.setEnabled(true);
           // stopScan.setEnabled(false);
           // setProgressBarIndeterminateVisibility(false);
            Intent intent = new Intent(getBaseContext(), WiFiCollectorService.class);
            intent.putExtra(WiFiCollectorService.REQUEST_STRING, "STOP_SCAN");
        // startService(intent);
            stopService(intent);
       // }

        super.onDestroy();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "In onResume start_scan_on_boot: " + start_scan_on_boot);
        if ((start_scan_on_boot == true) && (isServiceRunning() == false))  {
            //startScan.setEnabled(false);
            //stopScan.setEnabled(true);
            setProgressBarIndeterminateVisibility(true);
            Intent intent = new Intent(getBaseContext(), WiFiCollectorService.class);
            intent.putExtra(WiFiCollectorService.REQUEST_STRING, "START_SCAN");
            startService(intent);
        }
        super.onResume();
    }

    private boolean isServiceRunning() {
        ActivityManager manager = (ActivityManager)getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (WiFiCollectorService.class.equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            switch (position) {
                case 0 : {
                    fragment = new SSIDBased();
                    Bundle args = new Bundle();
                    args.putInt(SSIDBased.ARG_SECTION_NUMBER, position + 1);
                    fragment.setArguments(args);
                    break;
                }
                default : {
                    fragment = new AccessPoints();
                    Bundle args = new Bundle();
                    args.putInt(AccessPoints.ARG_SECTION_NUMBER, position + 1);
                    fragment.setArguments(args);
                    break;
                }
            }
            return fragment;
        }

        @Override
        public int getCount() {
            // Show 3 total page
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.ssid_based).toUpperCase(l);
                case 1:
                    return getString(R.string.access_points).toUpperCase(l);
            }
            return null;
        }
    }

    class WiFiCollectorReceiver extends BroadcastReceiver {

        public static final String PROCESS_RESPONSE = "com.example.wifidetailed.intent.action.PROCESS_RESPONSE";
        private boolean started = false;
        public void onReceive(Context c, Intent intent) {
           // Log.d(TAG, "In onReceive Main");
            Bundle b = intent.getBundleExtra(WiFiCollectorService.RESPONSE_STRING);
            //  String responseMessage = intent.getStringExtra(WiFiCollectorService.RESPONSE_MESSAGE);
            if (b != null) {
                SSIDInfo ssidInfo = (SSIDInfo)b.getSerializable("SSIDInfo");

                if (ssidInfo == null) {
                    Log.d(TAG, "ssidInfo : null");
                }
// Update SSID ListView
                boolean found = false;
                for(int j=0; j< SSIDInfoList.size(); j++) {
                    if (SSIDInfoList.get(j).ssid.equalsIgnoreCase(ssidInfo.ssid)) {
                        SSIDInfoList.set(j,ssidInfo);
                        found = true;
                        break;
                    }
                }
                if (found == false) SSIDInfoList.add(ssidInfo);
                ListView ssid_items = (ListView) findViewById(R.id.listView_ssid);

                ssid_items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        SSIDInfo sd = SSIDInfoList.get(position);
                        //Toast.makeText(getBaseContext(), sd.ssid, 1000).show();

                        //builder.setTitle("SSID Details");
                        //builder.setMessage("is not a valid input!");

                        // get prompts.xml view
                        LayoutInflater li = LayoutInflater.from(MainActivity.this);
                        View sView = li.inflate(R.layout.ssid_details, null);
                        // ContextThemeWrapper cw = new ContextThemeWrapper( MainActivity.this, R.style.AlertDialogTheme );
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(sView);
                       // builder.setTitle(sd.ssid);

                        TextView tv = (TextView)sView.findViewById(R.id.ssid_details_name);
                        tv.setText(sd.ssid);
                        List<String> ssidDetails_list = new ArrayList<String>();
                        //ArrayAdapter<String> ssidDetails_list = new ArrayList<String>() ;

                        ssidDetails_list.add("MAC address :");
                        for(int k=0; k<APInfoList.size(); k++) {
                            if (sd.ssid.equalsIgnoreCase(APInfoList.get(k).ssid)) {
                                ssidDetails_list.add("  " + APInfoList.get(k).bssid +
                                        " Channel: " + APInfoList.get(k).channel +
                                        " Band: " + APInfoList.get(k).frequency);
                            }
                        }
                        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.simple_row, ssidDetails_list);
                        ListView lv = (ListView)sView.findViewById(R.id.ssid_detail_list_view);

                        lv.setAdapter(listAdapter);
                        /*
                        final CharSequence[] charSequenceItems = ssidDetails_list.toArray(new CharSequence[ssidDetails_list.size()]);
                        builder.setItems(charSequenceItems, null);
                        */
                        builder.setPositiveButton(android.R.string.ok, null);


                        AlertDialog alert = builder.create();
                        //TextView tv = (TextView)alert.findViewById(android.R.id.title);
                        //tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        alert.show();
                    }

                });
                ssid_items.setAdapter(ssid_adapter);
                ssid_adapter.notifyDataSetChanged();

// Update AP ListView
                found = false;
                for(int j=0; j< APInfoList.size(); j++) {
                    if (APInfoList.get(j).bssid.equalsIgnoreCase(ssidInfo.bssid)) {
                        found = true;
                        break;
                    }
                }
                if (found == false) {
                    APInfo ap_info = new APInfo();
                    ap_info.bssid = ssidInfo.bssid;
                    ap_info.ssid= ssidInfo.ssid;
                    ap_info.frequency = ssidInfo.frequency;
                    ap_info.channel = ssidInfo.channel;
                    ap_info.vendor = ssidInfo.vendor;
                    //ap_info.index = j;
                    APInfoList.add(ap_info);
                }
                ListView ap_items = (ListView) findViewById(R.id.listView_access_point);
                ap_items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        APInfo ap = APInfoList.get(position);
                        //Toast.makeText(getBaseContext(), sd.ssid, 1000).show();

                        //builder.setTitle("SSID Details");
                        //builder.setMessage("is not a valid input!");

                        // get prompts.xml view
                        LayoutInflater li = LayoutInflater.from(MainActivity.this);
                        View sView = li.inflate(R.layout.access_point_details, null);
                        // ContextThemeWrapper cw = new ContextThemeWrapper( MainActivity.this, R.style.AlertDialogTheme );
                        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(sView);
                        // builder.setTitle(sd.ssid);
                        TextView tv = (TextView)sView.findViewById(R.id.ap_details_name);
                        tv.setText(ap.vendor + " (" + ap.bssid + ")");
                        List<String> apDetails_list = new ArrayList<String>();
                        //ArrayAdapter<String> ssidDetails_list = new ArrayList<String>() ;

                        apDetails_list.add("SSID List:");
                        apDetails_list.add("  " + ap.ssid);
                        for(int k=0; k<SSIDInfoList.size(); k++) {
                            if (ap.bssid.equalsIgnoreCase(SSIDInfoList.get(k).bssid)) {
                                if (ap.ssid.equalsIgnoreCase(SSIDInfoList.get(k).ssid) == false)
                                                apDetails_list.add("  " + SSIDInfoList.get(k).ssid);
                            }
                        }
                        ArrayAdapter<String> listAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.simple_row, apDetails_list);
                        ListView lv = (ListView)sView.findViewById(R.id.ap_detail_list_view);

                        lv.setAdapter(listAdapter);
                        /*
                        final CharSequence[] charSequenceItems = ssidDetails_list.toArray(new CharSequence[ssidDetails_list.size()]);
                        builder.setItems(charSequenceItems, null);
                        */
                        builder.setPositiveButton(android.R.string.ok, null);


                        AlertDialog alert = builder.create();
                        //TextView tv = (TextView)alert.findViewById(android.R.id.title);
                        //tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
                        alert.requestWindowFeature(Window.FEATURE_NO_TITLE);

                        alert.show();
                    }

                });
                ap_items.setAdapter(ap_adapter);
                ap_adapter.notifyDataSetChanged();

            } else {
                Log.d(TAG, "Extras is null");
            }

/*
            wifi_items.setOnItemClickListener( new getActivity().OnItemClickListener() {
                public void onItemClick(AdapterView <?> arg0, View arg1, int arg2, long arg3) {
                    Intent i = new Intent(getActivity().this,);
                    startActivity(i);
                });
            }
            */
        }
    }
}
