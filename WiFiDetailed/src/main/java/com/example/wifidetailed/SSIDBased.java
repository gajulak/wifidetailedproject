package com.example.wifidetailed;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vgajula on 6/11/13.
 */
public class SSIDBased extends Fragment {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private static final String TAG = "WiFiConn.WiFiDemo";
    public SSIDBased() { }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;
        rootView = inflater.inflate(R.layout.ssid_based, container, false);
        return rootView;
    }
}
