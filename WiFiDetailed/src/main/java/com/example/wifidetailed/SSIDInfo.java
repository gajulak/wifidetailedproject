package com.example.wifidetailed;

import java.io.Serializable;

/**
 * Created by vgajula on 7/8/13.
 */
public class SSIDInfo implements Serializable {
    public int index;
    public String ssid;
    public int ap_count;
    public String bssid;
    public int channel;
    public String frequency;
    public String vendor;
}
