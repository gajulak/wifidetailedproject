package com.example.wifidetailed;

import android.net.wifi.ScanResult;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by vgajula on 6/11/13.
 */

public class WiFiConnection {
    public String ssid;
    public int count;
    ArrayList<WiFiRadio> radio;
    //public ArrayList<String> macAddress;
    final public String TAG = "WiFiConn.WiFiConnection";

    public WiFiConnection(){
        this.count = 1;
        this.radio = new ArrayList<WiFiRadio>();
    }

    public void addDetails(ScanResult result) {
       WiFiRadio r = new WiFiRadio();
       r.bssid = result.BSSID.toString();
       r.frequency = result.frequency;
       //Log.d(TAG, "BSSID : " + r.bssid + "Freq" + r.frequency);
       radio.add(r);
    }

    public boolean getBSSID(String bssid) {
        for(int i = 0; i < radio.size(); i++) {
            if (radio.get(i).bssid.equalsIgnoreCase(bssid)) return true;
        }
        return false;
    }

    public class WiFiRadio {
        public String bssid;
        public int frequency;
        public WiFiRadio() {}
    }
}
