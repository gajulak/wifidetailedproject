package com.example.wifidetailed;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import java.util.List;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;

/**
 * Created by vgajula on 7/10/13.
 */
public class Settings extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    private final static String TAG = "WiFiConn.Settings";
    private CheckBoxPreference mScanCheckbox;
    private EditTextPreference mScanInterval;
    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        PreferenceManager.setDefaultValues(Settings.this, R.xml.preferences, false);
        mScanCheckbox = (CheckBoxPreference) getPreferenceScreen()
                .findPreference("scanOnStartCheckbox");
        mScanInterval = (EditTextPreference) getPreferenceScreen()
                .findPreference("scan_interval");
        mScanInterval.setOnPreferenceChangeListener(
                new Preference.OnPreferenceChangeListener() {

                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        String val = (String)newValue;
                        if (Integer.parseInt(val) > 0) {
                            //Toast.makeText(preference, "Invalid value: 0", Toast.LENGTH_SHORT).show();
                            return true;
                        }
                        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Invalid Input");
                        builder.setMessage(val + " is not a valid input!");
                        builder.setPositiveButton(android.R.string.ok, null);
                        builder.show();
                        return false;
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mScanInterval.setSummary("Current value is "
                + mScanInterval.getText() + " secs");
        mScanCheckbox.setSummary(mScanCheckbox.isChecked() ? "On" : "Off");
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener whenever a key changes
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        // Let's do something when a preference value changes
        if (key.equals("scan_interval")) {
            // String val = sharedPreferences.getString(key, "");
            //if (Integer.parseInt(val) > 0) {
                mScanInterval.setSummary("Current value is "
                    + sharedPreferences.getString(key, "") + " secs");
            //} else {
               // Toast.makeText(this, "Invalid value: 0", Toast.LENGTH_SHORT).show();
            //}
        } else if (key.equals("scanOnStartCheckbox")) {
            mScanCheckbox.setSummary(sharedPreferences.getBoolean(key,
                    false) ? "On" : "Off");
        }
    }


   /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed Called");
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
    */
}
