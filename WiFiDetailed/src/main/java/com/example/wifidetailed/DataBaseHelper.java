package com.example.wifidetailed;

import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by vgajula on 7/9/13.
 */
public class DataBaseHelper extends SQLiteOpenHelper {

    private SQLiteDatabase sqliteDB;
    private final Context myContext;
    private final String TAG = "WiFiConn.DataBaseHelper";
    private static String DB_PATH = "/data/data/" + "com.example.wifidetailed" + "/databases/";
    private static String DB_NAME = "mac_address_db.db";

    public DataBaseHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.myContext = context;
    }

    public void createDataBase() throws IOException {
        Log.d(TAG, "createDataBase");
        File f = new File(DB_PATH);
        if (!f.exists()) {
            f.mkdir();
        }
        boolean dbExists = checkDataBase();
        if (dbExists) {
            return;
        } else {
            this.getReadableDatabase();
            try {
                copyDataBase();
            } catch (IOException e) {
               // throw new Error("Error copying database");
            }
        }
    }

    public boolean checkDataBase() {
       // Log.d(TAG, "checkDataBase");
        SQLiteDatabase checkDB = null;
        try{
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }catch(SQLiteException e){
           // throw new Error("Error database deosn't exist");
        }
        if(checkDB != null){
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDataBase() throws IOException{
       // Log.d(TAG, "copyDataBase");

        //Open your local db as the input stream
        AssetManager assetManager = myContext.getAssets();

        if (assetManager == null) {
            Log.d(TAG, "*** Assets: is NULL ****");
            return;
        }
        Log.d(TAG, "Assets: " + assetManager.list(DB_NAME).toString());
        InputStream myInput = assetManager.open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        Log.d(TAG, "openDataBase");
        //Open the database
        String myPath = DB_PATH + DB_NAME;
        sqliteDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
    }

    @Override
    public synchronized void close() {
        if(sqliteDB != null) sqliteDB.close();
        sqliteDB = null;
        super.close();
    }

    public String getSettings(String attr) {
        String p_query = "select value from settings where _id = '" + attr +"'";
        Cursor cursor = sqliteDB.rawQuery(p_query, null);

        if (cursor.moveToFirst()) {
            do {
                Log.d(TAG, "value: " + cursor.getString(0));
                return cursor.getString(0);
               // return value;
            } while (cursor.moveToNext());
        }
        return null;
    }

    public String getVendorInfo(String mac) {
        /*
        if (sqliteDB == null) {
            String myPath = DB_PATH + DB_NAME;
            sqliteDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        }
        */
        String mac_upper = mac.toUpperCase();
        String p_query = "select vendor_description from mac_address_table where _id = '" + mac_upper +"'";
        Cursor cursor = sqliteDB.rawQuery(p_query, null);

        // looping through all rows and adding to list
        String vendor = "Unknown";
        if (cursor.moveToFirst()) {
            do {
               vendor = cursor.getString(0);
               break;
            } while (cursor.moveToNext());
        }
        return vendor;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}
