package com.example.wifidetailed;

import android.app.IntentService;
//import android.app.Service;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
//import android.widget.ListView;
import android.widget.Toast;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.example.wifidetailed.MainActivity.WiFiCollectorReceiver;

/**
 * Created by vgajula on 7/7/13.
 */
public class WiFiCollectorService extends Service {
    private static final String TAG = "WiFiConn.WiFiCollectorService";
    WifiManager wifi;
    BroadcastReceiver receiverWifi;
    List<ScanResult> wifiList;
    List<WiFiConnection> WifiConn = new ArrayList<WiFiConnection>();
    private static boolean isRunning = false;
    private static boolean isScanRunning = false;
    DataBaseHelper myDbHelper;

    public static final String REQUEST_STRING = "myRequest";
    public static final String RESPONSE_STRING = "myResponse";
    public static final String RESPONSE_MESSAGE = "myResponseMessage";
    private Timer timer;
    private final static ArrayList<Integer> channelsFrequency =
            new ArrayList<Integer>(Arrays.asList(0, 2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447,
                    2452, 2457, 2462, 2467, 2472, 2484));

    private final static int start_frequency_5ghz = 5180;
    private final static int start_channel_5ghz = 36;

    private static boolean start_scan_on_boot = false;
    private static long scan_interval = 60; // secs


    private TimerTask updateTask = new TimerTask() {
        @Override
        public void run() {
            //Log.i(TAG, "Timer task doing work");
            wifi.startScan();
        }
    };


    @Override
    public void onCreate(){
        if (myDbHelper == null) {
            myDbHelper = new DataBaseHelper(getApplicationContext());

            try {
                myDbHelper.createDataBase();
            } catch (IOException ioe) {
                throw new Error("Unable to create database");
            }

            try {
                 myDbHelper.openDataBase();
            }catch(SQLException sqle){
                throw sqle;
         }

         SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
         String interval_str = prefs.getString("scan_interval", "2");
         start_scan_on_boot = prefs.getBoolean("scanOnStartCheckbox", false);

         //String start_scan_str = myDbHelper.getSettings("start_scan_on_boot");
         //String interval_str = myDbHelper.getSettings("scan_interval");
         scan_interval = Integer.parseInt(interval_str);
            Log.d(TAG, "scan_interval: " + scan_interval);

          //  Log.d(TAG, "onCreate :" + start_scan_str + " " + interval_str);
         if (start_scan_on_boot) {
                //Log.d(TAG, "Just before startScan :" + start_scan_str + " " + interval_str);
                startScan();
         }
        }
        super.onCreate();
    }

    private void startScan() {
        if (isScanRunning == true) return;
        Toast.makeText(this, "Wifi scan started", Toast.LENGTH_SHORT).show();
        wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo w = wifi.getConnectionInfo();
        Log.d(TAG, w.toString());

        receiverWifi = new WifiReceiver();
        registerReceiver(receiverWifi, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        timer = new Timer();
        scan_interval *= 1000;
        timer.schedule(updateTask, 0, scan_interval);
        isScanRunning = true;
        //wifi.startScan();
    }
    private void stopScan() {
        if (isScanRunning == false) return;
        isScanRunning = false;
        unregisterReceiver(receiverWifi);
        if (timer != null){
            timer.cancel();
            timer.purge();
            timer = null;
        }
        Toast.makeText(this, "Wifi scan stopped", Toast.LENGTH_SHORT).show();
    }


   public int onStartCommand(Intent intent, int flags, int startId) {

        Log.d(TAG, "Starting Service");
     //   super.onStartCommand(intent, flags, startId);
        String requestString = intent.getStringExtra(REQUEST_STRING);
        if (requestString != null) {
         Log.d(TAG, "ReqString " + requestString);
            if (requestString.equalsIgnoreCase("START_SCAN")) {
                startScan();
            } else if (requestString.equalsIgnoreCase("STOP_SCAN")) {
                stopScan();
            }
        }

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        stopScan();

        Log.d(TAG, "stopping service");
        //Toast.makeText(this, "WifiCollector Service Stopped", Toast.LENGTH_LONG).show();
    }
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    class WifiReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            wifiList = wifi.getScanResults();

            for(int i = 0; i < wifiList.size(); i++){
               // Log.d(TAG, (wifiList.get(i)).toString());
                int found = 0;
                int index = 0;
                for(int j=0; j< WifiConn.size(); j++, index++) {
                    if (WifiConn.get(j).ssid.equalsIgnoreCase(wifiList.get(i).SSID)) {
                       if (WifiConn.get(j).getBSSID(wifiList.get(i).BSSID.toString()) == false) {
                            WifiConn.get(j).count++;
                            WifiConn.get(j).addDetails(wifiList.get(i));
                            // String str = wifiList.get(i).BSSID.toString();
                            //WifiConn.get(j).macAddress.add(wifiList.get(i).BSSID.toString());
                            found = 1;

                           // Log.d(TAG,"SSIDInfo" + j);
                            SSIDInfo ssidInfo = new SSIDInfo();
                            ssidInfo.ap_count = WifiConn.get(j).count;
                            ssidInfo.ssid = WifiConn.get(j).ssid;
                            ssidInfo.bssid = wifiList.get(i).BSSID.toString();
                            ssidInfo.channel = channelsFrequency.indexOf(Integer.valueOf(wifiList.get(i).frequency));
                            ssidInfo.vendor = myDbHelper.getVendorInfo(ssidInfo.bssid.substring(0,8));
                           // Log.d(TAG, "Frequency " + wifiList.get(i).frequency );
                            if ( ssidInfo.channel > 0) {
                                //Log.d(TAG, "Frequency 2.4GHz");
                               ssidInfo.frequency = "2.4GHz";
                            }
                            else {
                                //Log.d(TAG, "Frequency 5GHz");
                               ssidInfo.channel = start_channel_5ghz + (wifiList.get(i).frequency-start_frequency_5ghz)/5;
                               ssidInfo.frequency = "5GHz";
                            }
                            ssidInfo.index = index;
                            sendSSIDInfoMessage(ssidInfo);
                            break;
                       }
                    }
                }
                if (found == 0) {
                    WiFiConnection conn;
                    conn = new WiFiConnection();
                    conn.ssid = wifiList.get(i).SSID.toString();
                    //conn.macAddress.add(wifiList.get(i).BSSID.toString());
                    conn.addDetails(wifiList.get(i));
                    WifiConn.add(conn);

                    SSIDInfo ssidInfo = new SSIDInfo();
                    ssidInfo.ap_count = conn.count;
                    ssidInfo.ssid = conn.ssid;
                    ssidInfo.bssid = wifiList.get(i).BSSID.toString();
                    ssidInfo.vendor = myDbHelper.getVendorInfo(ssidInfo.bssid.substring(0,8));


                   // Log.d(TAG, ">>>> Vendor " + ssidInfo.vendor );

                    ssidInfo.channel = channelsFrequency.indexOf(Integer.valueOf(wifiList.get(i).frequency));

                    if ( ssidInfo.channel > 0) {
                        //Log.d(TAG, "Frequency 2.4GHz");
                        ssidInfo.frequency = "2.4GHz";
                    }
                    else {
                        //Log.d(TAG, "Frequency 5GHz");
                        ssidInfo.channel = start_channel_5ghz + (wifiList.get(i).frequency-start_frequency_5ghz)/5;
                        ssidInfo.frequency = "5GHz";
                    }
                    ssidInfo.index = index;
                    sendSSIDInfoMessage(ssidInfo);
                }
            }

           // Log.d(TAG, ">>>>>>>>>>>>>>>>Wifi BroadCast Status Done!!<<<<<<<<");
        }

        void sendSSIDInfoMessage (SSIDInfo ssidInfo) {
            String responseString = "Test";
            String responseMessage = "SSID";
            Bundle b = new Bundle();
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(WiFiCollectorReceiver.PROCESS_RESPONSE);
            broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);

            b.putSerializable("SSIDInfo",ssidInfo);
            broadcastIntent.putExtra(RESPONSE_STRING, b);
           // broadcastIntent.putExtra(RESPONSE_STRING, responseString);
           // broadcastIntent.putExtra(RESPONSE_MESSAGE, responseMessage);
            sendBroadcast(broadcastIntent);
           // Log.d(TAG, "sent Message");
        }
    }
    public static Integer getFrequencyFromChannel(int channel) {
        return channelsFrequency.get(channel);
    }

    public static int getChannelFromFrequency(int frequency) {
        return channelsFrequency.indexOf(Integer.valueOf(frequency));
    }

}
